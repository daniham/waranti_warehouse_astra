#
# TABLE STRUCTURE FOR: detail
#

DROP TABLE IF EXISTS `detail`;

CREATE TABLE `detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_karyawan` varchar(100) NOT NULL,
  `nopol` varchar(100) NOT NULL,
  `model_kendaraan` varchar(100) NOT NULL,
  `vin_rangka` varchar(100) NOT NULL,
  `kilometer` varchar(100) NOT NULL,
  `tgl_perbaikan` date NOT NULL,
  `tgl_penyerahan` date NOT NULL,
  `no_part` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `barcode` varchar(100) NOT NULL,
  `lpd` varchar(100) NOT NULL,
  `no_rak` varchar(100) NOT NULL,
  `qr_code` varchar(100) NOT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(100) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `detail` (`id`, `nama_karyawan`, `nopol`, `model_kendaraan`, `vin_rangka`, `kilometer`, `tgl_perbaikan`, `tgl_penyerahan`, `no_part`, `deskripsi`, `barcode`, `lpd`, `no_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (4, 'DUDUNG', 'D90090AS', 'KAWASAKI', '981321938', '121221', '2020-05-01', '2020-05-31', 'D9T000-00214-001', 'COVER MIRROR CHROME EXT ASSY', '09080809988', '0909099', '999', 'Kawasaki.png', 'JULI', '2020-05-05', 'JULI', '2020-05-05');


#
# TABLE STRUCTURE FOR: karyawan
#

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `npk` int(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `email` varchar(35) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `status_karyawan` varchar(30) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(50) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(50) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO `karyawan` (`id`, `npk`, `nama_karyawan`, `jk`, `tgl_lahir`, `alamat`, `no_telp`, `no_ktp`, `email`, `tgl_masuk`, `status_karyawan`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (12, 1222, 'jawa orang', 'perempuan', '0000-00-00', 'solokan', '9128378', 1212321, 'dani@gmail.com', '0000-00-00', 'Tetap', '1222.png', 'JULI', '2020-05-05', NULL, NULL);
INSERT INTO `karyawan` (`id`, `npk`, `nama_karyawan`, `jk`, `tgl_lahir`, `alamat`, `no_telp`, `no_ktp`, `email`, `tgl_masuk`, `status_karyawan`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (13, 12, 'WULAN', 'perempuan', '2020-05-01', 'jatiluhur', '085715329867', 2147483647, 'wulammariaulfah@gmail.com', '2020-05-14', 'Kontrak', 'wulan.png', 'JULI', '2020-05-05', 'JULI', '2020-05-05');
INSERT INTO `karyawan` (`id`, `npk`, `nama_karyawan`, `jk`, `tgl_lahir`, `alamat`, `no_telp`, `no_ktp`, `email`, `tgl_masuk`, `status_karyawan`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (14, 12, 'DUDUNG', 'laki-laki', '2020-05-09', 'ksjdsd', '123123', 121212, 'hahahahah@gmail', '2020-05-16', 'Kontrak', '', '', '0000-00-00', NULL, NULL);


#
# TABLE STRUCTURE FOR: kendaraan
#

DROP TABLE IF EXISTS `kendaraan`;

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nopol` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `tipe_mesin` varchar(100) NOT NULL,
  `model_kendaraan` varchar(100) NOT NULL,
  `vin_rangka` varchar(100) NOT NULL,
  `kilometer` varchar(100) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(100) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `kendaraan` (`id`, `nopol`, `model`, `tipe_mesin`, `model_kendaraan`, `vin_rangka`, `kilometer`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (1, 'D65411', 'avanza', 'tipeuting', 'sdhagd', '19821928', '131313313', 'sdhagd.png', 'dani hamdani', '2020-04-12', 'dani hamdani', '2020-04-12');
INSERT INTO `kendaraan` (`id`, `nopol`, `model`, `tipe_mesin`, `model_kendaraan`, `vin_rangka`, `kilometer`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (3, '23222', 'awd', 'adw', 'awdadawd', '20193', '29833', '23222.png', 'dani hamdani', '2020-04-15', '', '0000-00-00');
INSERT INTO `kendaraan` (`id`, `nopol`, `model`, `tipe_mesin`, `model_kendaraan`, `vin_rangka`, `kilometer`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (5, 'D6242zah', 'd1525', 'yahaasd', 'ahdaman', '1920', '12466', 'D6242zah.png', 'dani hamdani', '2020-04-25', '', '0000-00-00');


#
# TABLE STRUCTURE FOR: level
#

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `level` (`id_level`, `nama_level`) VALUES (1, 'admin');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (2, 'checker');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (3, 'owner');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (4, 'pelanggan');


#
# TABLE STRUCTURE FOR: rak
#

DROP TABLE IF EXISTS `rak`;

CREATE TABLE `rak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rak` int(11) NOT NULL,
  `nama_rak` varchar(50) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(100) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (2, 111, 'kastilion', '0111.png', 'dani hamdani', '2020-04-11', 'dani hamdani', '2020-04-11');
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (4, 22, 'roki', '22.png', 'dani hamdani', '2020-04-11', 'dani hamdani', '2020-04-22');
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (5, 400, 'JULID', '400.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (6, 21, 'aaaaaaaaaaaaaaaaaaa', '21.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (7, 55, 'esien', '55.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (9, 89, 'suqi', '89.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (11, 56, 'dani hamdani', 'dani hamdani.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (12, 8999, 'lovanto jurig', 'lovanto jurig.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (13, 999, 'juriggg', 'juriggg.png', 'dani hamdani', '2020-04-11', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (16, 231242421, 'awdjhlnawd', '231242421.png', 'dani hamdani', '2020-04-15', NULL, NULL);
INSERT INTO `rak` (`id`, `no_rak`, `nama_rak`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (17, 99, 'yuyuk', '99.png', 'dani hamdani', '2020-04-21', NULL, NULL);


#
# TABLE STRUCTURE FOR: sparepart
#

DROP TABLE IF EXISTS `sparepart`;

CREATE TABLE `sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_part` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (1, 'D04465-BZ153-000', 'PAD KIT, DISC BRAKE ,BRACKER', 'D04465-BZ153-000.png', 'dani hamdani', '2020-04-11', 'dani hamdani', '2020-04-11');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (2, 'D04478-BZ060-001', 'PAD KIT, DISC BRAKE', 'D04478-BZ060-001.png', 'dani hamdani', '2020-04-11', 'dani hamdani', '2020-04-22');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (5, '90111', 'gelo', '90111.png', 'dani hamdani', '2020-04-21', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (6, 'D9T000-00196-001', 'DOOR SWITCH TERIOS TS 708', 'D9T000-00196-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (7, 'D9T000-00197-001', 'HARNESS SET ALARM TERIOSTX 708', 'D9T000-00197-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (8, 'D9T000-00198-001', 'HARNESS SET ALARM TERIOSTS 708', 'D9T000-00198-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (9, 'D9T000-00211-001', 'ALARM NEW DESIGN 1207 TX', 'D9T000-00211-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (10, 'D9T000-00212-001', 'FOOT STEP NEW ELEGAN 1207 ASSY', 'D9T000-00212-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (11, 'D9T000-00212-021', 'FOOT STEP NEW ELEGANT 1207 R/H', 'D9T000-00212-021.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (12, 'D9T000-00212-031', 'FOOT STEP NEW ELEGANT 1207 L/H', 'D9T000-00212-031.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (13, 'D9T000-00213-001', 'SPARE WHEEL COVER EXO BEIGE M', 'D9T000-00213-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (14, 'D9T000-00213-002', 'SPARE WHEEL COVER SPAR BLUE M', 'D9T000-00213-002.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (15, 'D9T000-00213-003', 'SPARE WHEEL COVER CLAS SILVR M', 'D9T000-00213-003.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (16, 'D9T000-00213-004', 'SPARE WHEEL COVER MID BLACK M', 'D9T000-00213-004.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (17, 'D9T000-00213-005', 'SPARE WHEEL COVER WINE RED M', 'D9T000-00213-005.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (18, 'D9T000-00214-001', 'COVER MIRROR CHROME EXT ASSY', 'D9T000-00214-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (19, 'D9T000-00214-011', 'COVER MIRROR CHROME EXTRA L/H', 'D9T000-00214-011.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (20, 'D9T000-00214-021', 'COVER MIRROR CHROME EXTRA R/H', 'D9T000-00214-021.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (21, 'D9T000-00215-001', 'FOG LAMP EXTRA ASSY', 'D9T000-00215-001.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (22, 'D9T000-00215-011', 'FOG LAMP EXTRA L/H', 'D9T000-00215-011.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (23, 'D9T000-00215-021', 'FOG LAMP EXTRA R/H', 'D9T000-00215-021.png', 'JULI', '2020-05-05', '', '0000-00-00');
INSERT INTO `sparepart` (`id`, `no_part`, `deskripsi`, `qr_code`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (24, 'D9T000-00216-001', 'COV FOG LAMP NEW ELEG ASSY', 'D9T000-00216-001.png', 'JULI', '2020-05-05', '', '0000-00-00');


#
# TABLE STRUCTURE FOR: user
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_user` varchar(20) NOT NULL,
  `id_level` int(20) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (1, 'dani', 'wulan', 'dani hamdani', 1, '', '0000-00-00', 'dani hamdani', '2020-04-22');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (8, 'ouu', 'dani', 'juara', 2, '', '0000-00-00', 'dani hamdani', '2020-01-24');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (9, 'gila', 'dani', 'juara', 2, 'dani hamdani', '2020-01-24', 'dani hamdani', '2020-01-25');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (10, 'Wulantok', 'wulan', 'Wulantok', 2, 'dani hamdani', '2020-02-21', 'dani hamdani', '2020-04-22');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (11, 'jovi', 'jovi', 'juara', 1, 'dani hamdani', '2020-04-22', '', '0000-00-00');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (12, 'JULI ', '160791a', 'JULI', 1, 'dani hamdani', '2020-04-25', 'dani hamdani', '2020-05-01');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (13, 'juli', '123456', 'juli', 1, 'dani hamdani', '2020-05-01', '', '0000-00-00');


