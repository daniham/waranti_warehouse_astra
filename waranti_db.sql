/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.9-MariaDB : Database - waranti_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`waranti_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `waranti_db`;

/*Table structure for table `detail` */

DROP TABLE IF EXISTS `detail`;

CREATE TABLE `detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_karyawan` varchar(100) NOT NULL,
  `nopol` varchar(100) NOT NULL,
  `model_kendaraan` varchar(100) NOT NULL,
  `vin_rangka` varchar(100) NOT NULL,
  `kilometer` varchar(100) NOT NULL,
  `tgl_perbaikan` date NOT NULL,
  `tgl_penyerahan` date NOT NULL,
  `no_part` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `barcode` varchar(100) NOT NULL,
  `lpd` varchar(100) NOT NULL,
  `no_rak` varchar(100) NOT NULL,
  `qr_code` varchar(100) NOT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(100) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `detail` */

insert  into `detail`(`id`,`nama_karyawan`,`nopol`,`model_kendaraan`,`vin_rangka`,`kilometer`,`tgl_perbaikan`,`tgl_penyerahan`,`no_part`,`deskripsi`,`barcode`,`lpd`,`no_rak`,`qr_code`,`user_create`,`create_date`,`user_update`,`update_date`) values (4,'DUDUNG','D90090AS','KAWASAKI','981321938','121221','2020-05-01','2020-05-31','D9T000-00214-001','COVER MIRROR CHROME EXT ASSY','09080809988','0909099','999','Kawasaki.png','JULI','2020-05-05','JULI','2020-05-05');

/*Table structure for table `karyawan` */

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `npk` int(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `email` varchar(35) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `status_karyawan` varchar(30) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(50) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(50) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `karyawan` */

insert  into `karyawan`(`id`,`npk`,`nama_karyawan`,`jk`,`tgl_lahir`,`alamat`,`no_telp`,`no_ktp`,`email`,`tgl_masuk`,`status_karyawan`,`qr_code`,`user_create`,`create_date`,`user_update`,`update_date`) values (12,1222,'jawa orang','perempuan','0000-00-00','solokan','9128378',1212321,'dani@gmail.com','0000-00-00','Tetap','1222.png','JULI','2020-05-05',NULL,NULL),(13,12,'WULAN','perempuan','2020-05-01','jatiluhur','085715329867',2147483647,'wulammariaulfah@gmail.com','2020-05-14','Kontrak','wulan.png','JULI','2020-05-05','JULI','2020-05-05'),(14,12,'DUDUNG','laki-laki','2020-05-09','ksjdsd','123123',121212,'hahahahah@gmail','2020-05-16','Kontrak','','','0000-00-00',NULL,NULL);

/*Table structure for table `kendaraan` */

DROP TABLE IF EXISTS `kendaraan`;

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nopol` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `tipe_mesin` varchar(100) NOT NULL,
  `model_kendaraan` varchar(100) NOT NULL,
  `vin_rangka` varchar(100) NOT NULL,
  `kilometer` varchar(100) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(100) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `kendaraan` */

insert  into `kendaraan`(`id`,`nopol`,`model`,`tipe_mesin`,`model_kendaraan`,`vin_rangka`,`kilometer`,`qr_code`,`user_create`,`create_date`,`user_update`,`update_date`) values (1,'D65411','avanza','tipeuting','sdhagd','19821928','131313313','sdhagd.png','dani hamdani','2020-04-12','dani hamdani','2020-04-12'),(3,'23222','awd','adw','awdadawd','20193','29833','23222.png','dani hamdani','2020-04-15','','0000-00-00'),(5,'D6242zah','d1525','yahaasd','ahdaman','1920','12466','D6242zah.png','dani hamdani','2020-04-25','','0000-00-00');

/*Table structure for table `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `level` */

insert  into `level`(`id_level`,`nama_level`) values (1,'admin'),(2,'checker'),(3,'owner'),(4,'pelanggan');

/*Table structure for table `rak` */

DROP TABLE IF EXISTS `rak`;

CREATE TABLE `rak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rak` int(11) NOT NULL,
  `nama_rak` varchar(50) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(100) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `rak` */

insert  into `rak`(`id`,`no_rak`,`nama_rak`,`qr_code`,`user_create`,`create_date`,`user_update`,`update_date`) values (2,111,'kastilion','0111.png','dani hamdani','2020-04-11','dani hamdani','2020-04-11'),(4,22,'roki','22.png','dani hamdani','2020-04-11','dani hamdani','2020-04-22'),(5,400,'JULID','400.png','dani hamdani','2020-04-11',NULL,NULL),(6,21,'aaaaaaaaaaaaaaaaaaa','21.png','dani hamdani','2020-04-11',NULL,NULL),(7,55,'esien','55.png','dani hamdani','2020-04-11',NULL,NULL),(9,89,'suqi','89.png','dani hamdani','2020-04-11',NULL,NULL),(11,56,'dani hamdani','dani hamdani.png','dani hamdani','2020-04-11',NULL,NULL),(12,8999,'lovanto jurig','lovanto jurig.png','dani hamdani','2020-04-11',NULL,NULL),(13,999,'juriggg','juriggg.png','dani hamdani','2020-04-11',NULL,NULL),(16,231242421,'awdjhlnawd','231242421.png','dani hamdani','2020-04-15',NULL,NULL),(17,99,'yuyuk','99.png','dani hamdani','2020-04-21',NULL,NULL);

/*Table structure for table `sparepart` */

DROP TABLE IF EXISTS `sparepart`;

CREATE TABLE `sparepart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_part` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `sparepart` */

insert  into `sparepart`(`id`,`no_part`,`deskripsi`,`qr_code`,`user_create`,`create_date`,`user_update`,`update_date`) values (1,'D04465-BZ153-000','PAD KIT, DISC BRAKE ,BRACKER','D04465-BZ153-000.png','dani hamdani','2020-04-11','dani hamdani','2020-04-11'),(2,'D04478-BZ060-001','PAD KIT, DISC BRAKE','D04478-BZ060-001.png','dani hamdani','2020-04-11','dani hamdani','2020-04-22'),(5,'90111','gelo','90111.png','dani hamdani','2020-04-21','','0000-00-00'),(6,'D9T000-00196-001','DOOR SWITCH TERIOS TS 708','D9T000-00196-001.png','JULI','2020-05-05','','0000-00-00'),(7,'D9T000-00197-001','HARNESS SET ALARM TERIOSTX 708','D9T000-00197-001.png','JULI','2020-05-05','','0000-00-00'),(8,'D9T000-00198-001','HARNESS SET ALARM TERIOSTS 708','D9T000-00198-001.png','JULI','2020-05-05','','0000-00-00'),(9,'D9T000-00211-001','ALARM NEW DESIGN 1207 TX','D9T000-00211-001.png','JULI','2020-05-05','','0000-00-00'),(10,'D9T000-00212-001','FOOT STEP NEW ELEGAN 1207 ASSY','D9T000-00212-001.png','JULI','2020-05-05','','0000-00-00'),(11,'D9T000-00212-021','FOOT STEP NEW ELEGANT 1207 R/H','D9T000-00212-021.png','JULI','2020-05-05','','0000-00-00'),(12,'D9T000-00212-031','FOOT STEP NEW ELEGANT 1207 L/H','D9T000-00212-031.png','JULI','2020-05-05','','0000-00-00'),(13,'D9T000-00213-001','SPARE WHEEL COVER EXO BEIGE M','D9T000-00213-001.png','JULI','2020-05-05','','0000-00-00'),(14,'D9T000-00213-002','SPARE WHEEL COVER SPAR BLUE M','D9T000-00213-002.png','JULI','2020-05-05','','0000-00-00'),(15,'D9T000-00213-003','SPARE WHEEL COVER CLAS SILVR M','D9T000-00213-003.png','JULI','2020-05-05','','0000-00-00'),(16,'D9T000-00213-004','SPARE WHEEL COVER MID BLACK M','D9T000-00213-004.png','JULI','2020-05-05','','0000-00-00'),(17,'D9T000-00213-005','SPARE WHEEL COVER WINE RED M','D9T000-00213-005.png','JULI','2020-05-05','','0000-00-00'),(18,'D9T000-00214-001','COVER MIRROR CHROME EXT ASSY','D9T000-00214-001.png','JULI','2020-05-05','','0000-00-00'),(19,'D9T000-00214-011','COVER MIRROR CHROME EXTRA L/H','D9T000-00214-011.png','JULI','2020-05-05','','0000-00-00'),(20,'D9T000-00214-021','COVER MIRROR CHROME EXTRA R/H','D9T000-00214-021.png','JULI','2020-05-05','','0000-00-00'),(21,'D9T000-00215-001','FOG LAMP EXTRA ASSY','D9T000-00215-001.png','JULI','2020-05-05','','0000-00-00'),(22,'D9T000-00215-011','FOG LAMP EXTRA L/H','D9T000-00215-011.png','JULI','2020-05-05','','0000-00-00'),(23,'D9T000-00215-021','FOG LAMP EXTRA R/H','D9T000-00215-021.png','JULI','2020-05-05','','0000-00-00'),(24,'D9T000-00216-001','COV FOG LAMP NEW ELEG ASSY','D9T000-00216-001.png','JULI','2020-05-05','','0000-00-00');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_user` varchar(20) NOT NULL,
  `id_level` int(20) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`nama_user`,`id_level`,`user_create`,`create_date`,`user_update`,`update_date`) values (1,'dani','wulan','dani hamdani',1,'','0000-00-00','dani hamdani','2020-04-22'),(8,'ouu','dani','juara',2,'','0000-00-00','dani hamdani','2020-01-24'),(9,'gila','dani','juara',2,'dani hamdani','2020-01-24','dani hamdani','2020-01-25'),(10,'Wulantok','wulan','Wulantok',2,'dani hamdani','2020-02-21','dani hamdani','2020-04-22'),(11,'jovi','jovi','juara',1,'dani hamdani','2020-04-22','','0000-00-00'),(12,'JULI ','160791a','JULI',1,'dani hamdani','2020-04-25','dani hamdani','2020-05-01'),(13,'juli','123456','juli',1,'dani hamdani','2020-05-01','','0000-00-00');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

/*!50001 DROP VIEW IF EXISTS `login` */;
/*!50001 DROP TABLE IF EXISTS `login` */;

/*!50001 CREATE TABLE  `login`(
 `id_user` int(11) ,
 `username` varchar(10) ,
 `password` varchar(20) ,
 `nama_user` varchar(20) ,
 `user_create` varchar(50) ,
 `create_date` date ,
 `user_update` varchar(50) ,
 `update_date` date ,
 `id_level` int(11) ,
 `nama_level` varchar(20) 
)*/;

/*View structure for view login */

/*!50001 DROP TABLE IF EXISTS `login` */;
/*!50001 DROP VIEW IF EXISTS `login` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login` AS select `user`.`id_user` AS `id_user`,`user`.`username` AS `username`,`user`.`password` AS `password`,`user`.`nama_user` AS `nama_user`,`user`.`user_create` AS `user_create`,`user`.`create_date` AS `create_date`,`user`.`user_update` AS `user_update`,`user`.`update_date` AS `update_date`,`level`.`id_level` AS `id_level`,`level`.`nama_level` AS `nama_level` from (`user` left join `level` on((`user`.`id_level` = `level`.`id_level`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
